//
//  ViewController.swift
//  S02Ex01
//
//  Created by Ilhan Seven on 22/05/16.
//  Copyright © 2016 egas. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    
    @IBOutlet weak var redGiftBox: UIImageView!
    @IBOutlet weak var blueGiftBox: UIImageView!
    @IBOutlet weak var redButton: UIButton!
    @IBOutlet weak var blueButton: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func redBoxControl(sender: AnyObject) {
        if redGiftBox.hidden {
            redGiftBox.hidden = false
            redButton.setTitle("Hide Red Box", forState:.Normal)
        } else {
            redGiftBox.hidden = true
            redButton.setTitle("Show Red Box", forState:.Normal)
        }
    }
    
    @IBAction func blueBoxControl(sender: AnyObject) {
        if blueGiftBox.hidden  {
            blueGiftBox.hidden = false
            blueButton.setTitle("Hide Blue Box", forState: .Normal)
        } else {
            blueGiftBox.hidden = true
            blueButton.setTitle("Show Blue Box", forState: .Normal)
        }
    }
}

